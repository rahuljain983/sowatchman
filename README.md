sowatchman
==========

StackOverflow watchman is a chrome extension which lets user to watch a question and notifies him about a new comment or answer.

Steps to run the extension:
----------------------

<strong>Step1:</strong> Download the extension code on your machine <br/>
(Using git): git clone https://github.com/blunderboy/sowatchman.git some_directory_name <br/>
(Download ZIP): You can also download the extension in zip format from  [Stackeye Repsitory](https://github.com/blunderboy/sowatchman)

<strong>Step2:</strong> Browse chrome://extensions/

<strong>Step3:</strong> Select Checkbox for Developer Mode

<strong>Step4:</strong> Click on Load unpacked extension

<strong>Step5:</strong> Browse the directory in which extension code is present

Author
------------------------------

Please feel free to drop a mail to sachinjain024@gmail.com in case you find any issues.
